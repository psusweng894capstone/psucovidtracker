module.exports = {
  name: 'develop',
  verbose: true,
  preset: 'jest-playwright-preset',
  setupFilesAfterEnv: ['./jestSetup.js'],
};
