"""
main.py.

Set up flask web server and routes.
This is the start of the program.
"""
from flask import Flask
from flask import render_template, request, jsonify
from flask_json import FlaskJSON, JsonError, json_response, as_json
from flask_compress import Compress
from apscheduler.schedulers.background import BackgroundScheduler
import datetime
import secrets
import app.api.cdc_api as cdc_api
import app.api.bing_api as bing_api

app = Flask(__name__, static_url_path='/static')
app.config['SECRET_KEY'] = secrets.token_urlsafe()
json = FlaskJSON(app)
Compress(app)

print("Starting Data Import")
covid_data = bing_api.bing_data()
print("Data Import Complete!")


def refresh_data(test=False):
    """
    Run all the functions and methods to refresh back-end data.

    :param test: Flag indicating whether function is run in a test mode.
        (Default is false)
    :returns: Nothing. Data is refreshed in place.
    """
    global covid_data
    print("UPDATING!")
    covid_data.update(test=test)


# Schedule data refresh to occur daily
scheduler = BackgroundScheduler()
scheduler.add_job(refresh_data, 'interval', days=1)
scheduler.start()


# Use decorator to tell Flask to correlate get_state_data function
# with specified URL
@app.route('/<string:state>', methods=['GET'])
def get_state_data(state):
    """
    Render the US State COVID data page to the user.

    :param state: Which state to return to user
    :return: HTML webpage to user
    """
    print("\t", datetime.datetime.now(), request.remote_addr,
          "USState",  state)
    return render_template(r'states.html', USState=state,
                           timestamp=datetime.datetime.now())


# Use decorator to tell Flask to correlate index function with specified URL
@app.route('/', methods=['GET'])
@app.route('/index', methods=['GET'])
def index():
    """
    Render the homepage to the user.

    :return: HTML webpage to user
    """
    # Print data to console for logging
    print("\t", datetime.datetime.now(), request.remote_addr, "INDEX")
    # Return search form to user
    return render_template(r'index.html', timestamp=datetime.datetime.now())


@app.route('/api/cdc', methods=['GET'])
@as_json
def get_cdc_api():
    """
    Get the cdc api data.

    :return: Returns the api data in json format
    """
    yesterday = datetime.date.today() - datetime.timedelta(days=1)
    return json_response(data=cdc_api.get_cdc_data(
        yesterday.year, yesterday.month, yesterday.day).to_json()
        )


@app.route('/api/regions', methods=['GET'])
def get_regions():
    """
    Get the region data.

    :return: Returns the region data in json format
    """
    response = covid_data.get_regions().fillna('')
    return jsonify(response.to_dict(orient='records'))


@app.route('/api/daterange', methods=['GET'])
def get_date_range():
    """
    Get the range of dates from the data.

    :return: Returns the range of dates from the data in json format
    """
    start_date = covid_data.get_oldest_date()
    end_date = covid_data.get_most_recent_date()
    reply = {'start_date': {'year': start_date.year,
                            'month': start_date.month,
                            'day': start_date.day
                            },
             'end_date': {'year': end_date.year,
                          'month': end_date.month,
                          'day': end_date.day
                          }
             }
    return jsonify(reply)


@app.route('/api/categories', methods=['GET'])
def get_categories():
    """
    Get the categories from the data.

    :return: Returns the categories from the data in json format
    """
    return jsonify(covid_data.get_categories())


@app.route('/api/dataset.geojson', methods=['GET'])
def get_geojson():
    """
    Get the geojson data.

    :return: Returns the geojson data
    """
    return covid_data.get_geojson_data()


@app.route('/api/locality.geojson', methods=['GET'])
def get_geojson_locality():
    """
    Get the geojson locality data.

    :return: Returns the geojson locality data
    """
    return covid_data.get_locality_data()


@app.route('/api/country.geojson', methods=['GET'])
def get_geojson_country():
    """
    Get the geojson country data.

    :return: Returns the geojson country data
    """
    return covid_data.get_country_data()


@app.route('/api/region.geojson', methods=['GET'])
def get_geojson_region():
    """
    Get the geojson region data.

    :return: Returns the geojson region data
    """
    return covid_data.get_region_data()


@app.route('/api/<string:column>/<int:num_pts>/<string:label>',
           methods=['GET'])
def get_chart_data(column, num_pts, label):
    """
    Get historical data.

    :return: Returns the geojson of historical data
    """
    agg = covid_data.get_aggregate_data(column, num_pts, label)
    return covid_data.df_to_geojson(agg)
