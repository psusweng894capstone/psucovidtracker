// eslint-disable-next-line no-undef
require(['esri/Map',
  'esri/views/MapView',
  'esri/widgets/Search',
  'esri/layers/FeatureLayer',
  'esri/layers/MapImageLayer',
  'esri/layers/GeoJSONLayer',
  'esri/popup/content/support/ChartMediaInfoValue',
  'esri/tasks/support/Query',
  'esri/widgets/TimeSlider'], function(Map, MapView, Search,
    Query, MapImageLayer, GeoJSONLayer, TimeSlider) {
  // the base map
  const map = new Map({
    basemap: 'dark-gray',
  });

  // the view to be displayed on the html page
  const view = new MapView({
    container: 'viewDiv',
    map: map,
    zoom: 5,
    center: [-90, 35], // longitude, latitude
    popup: {
      dockEnabled: true,
      dockOptions: {
        breakpoint: false,
        position: 'bottom-right',
      },
    },
  });

  view.constraints = {
    minScale: 100000000,
  };

  const stateLabels = {
    symbol: {
      type: 'text',
      color: [0, 255, 255, 0.2],
      font: {
        size: '14px',
        family: 'Noto Sans',
        weight: 'normal',
      },
    },
    labelPlacement: 'above-center',
    labelExpression: '[state_name]',
  };

  const statesRenderer = {
    type: 'simple', // autocasts as new SimpleRenderer()
    label: 'State Boundaries',
    symbol: {
      type: 'simple-fill',
      outline: {
        width: 0.5,
        color: [0, 255, 255, 0.2],
      },
    },
  };

  // the main feature layer for state data
  const boundaryMapImageLayer = new MapImageLayer({
    url:
        'https://sampleserver6.arcgisonline.com/arcgis/rest/services/USA/MapServer',
    sublayers: [
      {
        id: 2,
        renderer: statesRenderer,
        opacity: 1,
        title: 'State Boundaries',
        labelingInfo: [stateLabels],
        // popupTemplate: popupStateCovidInfo,
      }],
  });

  map.add(boundaryMapImageLayer);

  // Pop-Up Template when clicking on a data feature
  const template = {
    title: 'COVID-19 Info',
    content: [
      {
        type: 'fields',
        fieldInfos: [
          {fieldName: 'Country'},
          {
            fieldName: 'Region',
            label: 'State',
          },
          {fieldName: 'Locality'},
          {
            fieldName: 'new_cases',
            label: 'New Cases',
          },
          {
            fieldName: 'new_deaths',
            label: 'New Deaths',
          },
          {
            fieldName: 'Recovered',
            label: 'Recovered',
          },
          {
            fieldName: 'Confirmed',
            label: 'Total Cases',
          },
          {
            fieldName: 'Deaths',
            label: 'Total Deaths',
          },
          {
            fieldName: 'Updated',
            format: {
              dateFormat: 'short-date-short-time',
            },
          },
        ],
      },
      {
        type: 'media',
        mediaInfos: [
          {
            title: 'New Cases Trend:',
            type: 'line-chart',
            value: {
              fields: ['Day 0', 'Day 1', 'Day 2', 'Day 3', 'Day 4'],
            },
          },
          {
            title: 'Cases and Deaths:',
            type: 'column-chart',
            value: {
              fields: ['Confirmed', 'Deaths'],
            },
          },
          {
            title: 'Recoveries:',
            type: 'column-chart',
            value: {
              fields: ['Confirmed', 'Recovered'],
            },
          },
        ],
      },
    ],
  };
  // Pop-Up Template when clicking on a data feature
  const regionTemplate = {
    title: 'COVID-19 Info',
    content: [
      {
        type: 'fields',
        fieldInfos: [
          {fieldName: 'Country'},
          {
            fieldName: 'Region',
            label: 'State',
          },
          {fieldName: 'Locality'},
          {
            fieldName: 'new_cases',
            label: 'New Cases',
          },
          {
            fieldName: 'new_deaths',
            label: 'New Deaths',
          },
          {
            fieldName: 'Recovered',
            label: 'Recovered',
          },
          {
            fieldName: 'Confirmed',
            label: 'Total Cases',
          },
          {
            fieldName: 'Deaths',
            label: 'Total Deaths',
          },
          {
            fieldName: 'Currently Hospitalized',
            label: 'Currently Hospitalized',
          },
          {
            fieldName: 'Infection Fatality Ratio',
            label: 'Infection Fatality Ratio',
          },
          {
            fieldName: 'Case Fatality Ratio',
            label: 'Case Fatality Ratio',
          },
          {
            fieldName: 'Updated',
            format: {
              dateFormat: 'short-date-short-time',
            },
          },
        ],
      },
      {
        type: 'media',
        mediaInfos: [
          {
            title: 'New Cases Trend:',
            type: 'line-chart',
            value: {
              fields: ['Day 0', 'Day 1', 'Day 2', 'Day 3', 'Day 4'],
            },
          },
          {
            title: 'Cases and Deaths:',
            type: 'column-chart',
            value: {
              fields: ['Confirmed', 'Deaths'],
            },
          },
          {
            title: 'Recoveries:',
            type: 'column-chart',
            value: {
              fields: ['Confirmed', 'Recovered'],
            },
          },
        ],
      },
    ],
  };

  // Visual Variable for marker that sets up gradiant
  const colorVisualVariableLocality = {
    type: 'color',
    field: 'new_cases',
    stops: [
      {value: 0, color: '#22C01A'},
      {value: 150, color: '#FF1100'},
    ],
    legendOptions: {
      title: 'Total Cases',
    },
  };

  // Visual Variable for marker that sets up gradiant
  const colorVisualVariableRegion = {
    type: 'color',
    field: 'new_cases',
    stops: [
      {value: 0, color: '#22C01A'},
      {value: 1500, color: '#FF1100'},
    ],
    legendOptions: {
      title: 'Total Cases',
    },
  };

  // Visual Variable for marker that sets up gradiant
  const colorVisualVariableCountry = {
    type: 'color',
    field: 'new_cases',
    stops: [
      {value: 0, color: '#22C01A'},
      {value: 15000, color: '#FF1100'},
    ],
    legendOptions: {
      title: 'Total Cases',
    },
  };

  const rendererCountry = {
    type: 'simple',
    field: 'new_cases',
    symbol: {
      type: 'simple-marker',
      color: 'orange',
      outline: {
        color: 'white',
      },
    },
    visualVariables: [colorVisualVariableCountry],
  };

  const rendererLocality = {
    type: 'simple',
    field: 'new_cases',
    symbol: {
      type: 'simple-marker',
      color: 'orange',
      outline: {
        color: 'white',
      },
    },
    visualVariables: [colorVisualVariableLocality],
  };

  const rendererRegion = {
    type: 'simple',
    field: 'new_cases',
    symbol: {
      type: 'simple-marker',
      color: 'orange',
      outline: {
        color: 'white',
      },
    },
    visualVariables: [colorVisualVariableRegion],
  };

  // https://developers.arcgis.com/javascript/latest/sample-code/layers-geojson/index.html
  // https://developers.arcgis.com/javascript/latest/api-reference/esri-layers-GeoJSONLayer.html
  // https://geojson.org/
  // https://tools.ietf.org/html/rfc7946#section-1.1

  // If GeoJSON files are not on the same domain as your website,
  // a CORS enabled server or a proxy is required.
  const countryLayer = new GeoJSONLayer({
    title: 'Country',
    url: 'http://127.0.0.1:5000/api/country.geojson',
    copyright: 'Bing',
    popupTemplate: template,
    renderer: rendererCountry, // optional
    maxScale: 30000000,
  });

  const regionLayer = new GeoJSONLayer({
    title: 'Region',
    url: 'http://127.0.0.1:5000/api/region.geojson',
    copyright: 'Bing',
    popupTemplate: regionTemplate,
    renderer: rendererRegion, // optional
    minScale: 29000000,
    maxScale: 5000000,
  });

  const localityLayer = new GeoJSONLayer({
    title: 'Locality',
    url: 'http://127.0.0.1:5000/api/locality.geojson',
    copyright: 'Bing',
    popupTemplate: template,
    renderer: rendererLocality, // optional
    minScale: 5000000,
  });

  map.add(countryLayer);
  map.add(regionLayer);
  map.add(localityLayer);

  // add search bar UI control
  const searchWidget = new Search({view: view});
  view.ui.add(searchWidget, {position: 'top-right'});

  view.when(function() {
    // placeholder for when to perform actions after layer loads
  });


  let jsonData;
  let editFeature;

  // view.on("click", function (event) {
  //  // Unselect any currently selected features
  //  unselectFeature();
  //  // Listen for when the user clicks on the view
  //  view.hitTest(event).then(function (response) {
  //    // If user selects a feature, select it
  //    const results = response.results;
  //    if (
  //      results.length > 0 &&
  //      results[0].graphic &&
  //      results[0].graphic.layer === featureLayer
  //    ) {
  //      selectFeature(
  //        results[0].graphic.attributes[featureLayer.objectIdField]
  //      );

  /** */
  // function selectFeature(objectId) {
  //   // query feature from the server
  //   countryLayer
  //       .queryFeatures({
  //         objectIds: [objectId],
  //         outFields: ["*"],
  //         returnGeometry: true,
  //       });
  // }

  // Queries for all the Object IDs of features
  // matching the layer's configurations
  // e.g. definitionExpression
  // layer.queryObjectIds().then(function(results){
  // prints the array of Object IDs to the console
  //  console.log(results);
  // });

  $.getJSON('http://127.0.0.1:5000/api/region.geojson', function(data) {
    // JSON result in `data` variable
    jsonData = data;
    // window.alert(jsonData['columns']);
    JSON.stringify(jsonData);
  }).done(function() {
    // Queries for all the features matching the layer's outFields
    const query = countryLayer.createQuery();
    query.where = 'Country = \'United States\'';
    query.outFields = ['*'];
    countryLayer.queryFeatures(query).then(function(results) {
      editFeature = results.features[0];
      editFeature.attributes['Country'] = 'United States';
      // prints the array of result graphics to the console
      console.log(editFeature);

      // eslint-disable-next-line no-unused-vars
      const edits = {
        updateFeatures: [editFeature],
      };
      // applyEditsToLayer(edits);
    });
    console.log( 'update template success' );

    //* *   */
    // eslint-disable-next-line require-jsdoc, no-unused-vars
    function applyEditsToLayer(params) {
      // this needs to call each feature or an array
      // and change them individually
      // https://developers.arcgis.com/javascript/latest/sample-code/editing-groupedfeatureform/index.html
      // https://developers.arcgis.com/javascript/latest/sample-code/sandbox/index.html?sample=editing-groupedfeatureform
      countryLayer
          .applyEdits(params)
          .then(function(results) {
            const objectIds = [];
            results.updateFeatureResults.forEach(function(item) {
              objectIds.push(item.objectId);
            });
            // query the newly added features from the layer
            countryLayer
                .queryFeatures({
                  objectIds: objectIds,
                })
                .then(function(results) {
                  console.log(
                      results.features.length,
                      'features have been added.',
                  );
                });
          });
    }
    // Loop through updated attributes and assign
    // the updated values to feature attributes.
    // Object.keys(updated).forEach(function (name) {
    //   editFeature.attributes[name] = updated[name];
    // });
  });
});
