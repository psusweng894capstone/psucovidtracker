/* eslint-disable max-len */
// QAlity test for CVD-135: Zoom in and out

const {chromium} = require('playwright');
const expect = require('expect');
let browser;
let page;
beforeAll(async () => {
  browser = await chromium.launch();
});
afterAll(async () => {
  await browser.close();
});
beforeEach(async () => {
  page = await browser.newPage();
});
afterEach(async () => {
  await page.close();
});

it('Zooms the map in and out', async () => {
  // extend default timeout for page navigation
  page.setDefaultNavigationTimeout(600000);

  // extend default timeout for clicks
  page.setDefaultTimeout(600000);

  await page.goto('http://127.0.0.1:5000/');

  // Zoom out to get country data set
  // Click //div[normalize-space(.)='Zoom out' and normalize-space(@title)='Zoom out' and normalize-space(@role)='button']/span[1][normalize-space(@role)='presentation']
  await page.click('//div[normalize-space(.)=\'Zoom out\' and normalize-space(@title)=\'Zoom out\' and normalize-space(@role)=\'button\']/span[1][normalize-space(@role)=\'presentation\']');

  // TODO: Make this actually validate the layers being displayed
  // Click //canvas
  /*   await page.click('//canvas');

  const countryLayer = await page.$('div[data-layer-title="Country"]');
  expect(countryLayer.length > 0); */

  // Zoom  in to state level
  // Click //div[normalize-space(.)='Zoom in' and normalize-space(@title)='Zoom in' and normalize-space(@role)='button']/span[1][normalize-space(@role)='presentation']
  await page.click('//div[normalize-space(.)=\'Zoom in\' and normalize-space(@title)=\'Zoom in\' and normalize-space(@role)=\'button\']/span[1][normalize-space(@role)=\'presentation\']');

  // Click //div[normalize-space(.)='Zoom in' and normalize-space(@title)='Zoom in' and normalize-space(@role)='button']/span[1][normalize-space(@role)='presentation']
  await page.click('//div[normalize-space(.)=\'Zoom in\' and normalize-space(@title)=\'Zoom in\' and normalize-space(@role)=\'button\']/span[1][normalize-space(@role)=\'presentation\']');

  // Click //div[normalize-space(.)='Zoom in' and normalize-space(@title)='Zoom in' and normalize-space(@role)='button']/span[1][normalize-space(@role)='presentation']
  await page.click('//div[normalize-space(.)=\'Zoom in\' and normalize-space(@title)=\'Zoom in\' and normalize-space(@role)=\'button\']/span[1][normalize-space(@role)=\'presentation\']');

  // Click //canvas
  /*   await page.click('//canvas');

  const stateLayer = await page.$('div[data-layer-title="Region"]');
  expect(stateLayer.length > 0); */

  // Zoom in to county level
  // Click //div[normalize-space(.)='Zoom in' and normalize-space(@title)='Zoom in' and normalize-space(@role)='button']/span[1][normalize-space(@role)='presentation']
  await page.click('//div[normalize-space(.)=\'Zoom in\' and normalize-space(@title)=\'Zoom in\' and normalize-space(@role)=\'button\']/span[1][normalize-space(@role)=\'presentation\']');

  // Click //div[normalize-space(.)='Zoom in' and normalize-space(@title)='Zoom in' and normalize-space(@role)='button']/span[1][normalize-space(@role)='presentation']
  await page.click('//div[normalize-space(.)=\'Zoom in\' and normalize-space(@title)=\'Zoom in\' and normalize-space(@role)=\'button\']/span[1][normalize-space(@role)=\'presentation\']');

  // Click //canvas
  /*   await page.click('//canvas');

  const localityLayer = await page.$('div[data-layer-title="Locality"]');
  expect(localityLayer.length > 0); */
});
