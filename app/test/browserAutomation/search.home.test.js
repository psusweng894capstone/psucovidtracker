/* eslint-disable max-len */
// QAlity test for CVD-77: Search box

const {chromium} = require('playwright');
const expect = require('expect');
let browser;
let page;
beforeAll(async () => {
  browser = await chromium.launch();
});
afterAll(async () => {
  await browser.close();
});
beforeEach(async () => {
  page = await browser.newPage();
});
afterEach(async () => {
  await page.close();
});

it('Enters location and clicks search', async () => {
  // extend default timeout for page navigation
  page.setDefaultNavigationTimeout(600000);

  // extend default timeout for clicks
  page.setDefaultTimeout(600000);

  await page.goto('http://127.0.0.1:5000/');

  // Get Location Textbox
  await page.fill('[placeholder="Find address or place"]', 'Atlanta, GA');

  // Click Search button
  await page.click('[title="Search"]');

  // Assert Search Pop Up is correct
  const searchBoxText = await page.textContent('[class="esri-search-result-renderer__more-results-item"]');
  expect(searchBoxText === 'Atlanta, Georgia');
});
