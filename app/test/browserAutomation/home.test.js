// QAlity test for home page

const {chromium} = require('playwright');
const expect = require('expect');
let browser;
let page;
beforeAll(async () => {
  browser = await chromium.launch();
});
afterAll(async () => {
  await browser.close();
});
beforeEach(async () => {
  page = await browser.newPage();
});
afterEach(async () => {
  await page.close();
});

it('Loads Home Page', async () => {
  // extend default timeout for page navigation
  page.setDefaultNavigationTimeout(600000);

  // extend default timeout for clicks
  page.setDefaultTimeout(600000);

  await page.goto('http://127.0.0.1:5000/');

  // Assert Title
  expect(await page.title()).toBe('PSU COVID-19 Tracker');

  // Assert Masthead Banner title
  const bannerH1Content = await page.$('div[id=title]');
  expect(bannerH1Content === 'COVID-19 Tracker');

  // Assert Masthead Banner covid virus image
  const covidLogo = await page.$('div[id=covid_logo]');
  expect(covidLogo.length);

  // Assert Masthead Banner PSU icon
  const psuLogo = await page.$('div[id=psu_logo]');
  expect(psuLogo.length);

  // Assert Masthead Banner PSU icon
  const instructions = await page.$('div[id=instructions]');
  expect(instructions.length);

  // Assert Map Div
  const mapContent = await page.$('div[id=viewDiv]');
  expect(mapContent.length);
});
