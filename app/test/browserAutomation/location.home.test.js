/* eslint-disable max-len */
// QAlity test for CVD-32: Location Information

const {chromium} = require('playwright');
// const expect = require('expect');
let browser;
let page;
beforeAll(async () => {
  browser = await chromium.launch();
});
afterAll(async () => {
  await browser.close();
});
beforeEach(async () => {
  page = await browser.newPage();
});
afterEach(async () => {
  await page.close();
});

it('Enters location and clicks search', async () => {
  // extend default timeout for page navigation
  page.setDefaultNavigationTimeout(600000);

  // extend default timeout for clicks
  page.setDefaultTimeout(600000);

  // Go to http://127.0.0.1:5000/
  await page.goto('http://127.0.0.1:5000/');

  // Click input[aria-label="Search"]
  await page.click('input[aria-label="Search"]');

  // Fill input[aria-label="Search"]
  await page.fill('input[aria-label="Search"]', 'Seattle, WA');

  // Click text="Seattle, WA"
  await page.click('text="Seattle, WA"');

  // Click //canvas
  await page.click('//canvas');

  // TODO: Make this actually validate
  /*   // Click //div[normalize-space(@title)='Next feature' and normalize-space(@aria-label)='Next feature' and normalize-space(@role)='button']/span
  await page.click('//div[normalize-space(@title)=\'Next feature\' and normalize-space(@aria-label)=\'Next feature\' and normalize-space(@role)=\'button\']/span');

  // Click //div/div/div/div/div/div/div[normalize-space(.)='State Name: WashingtonCases: Deaths: Day: 10/04/2020']
  await page.click('//div/div/div/div/div/div/div[normalize-space(.)=\'State Name: WashingtonCases: Deaths: Day: 10/04/2020\']');

  // Click //div[normalize-space(@title)='Close' and normalize-space(@aria-label)='Close' and normalize-space(@role)='button']/span
  await page.click('//div[normalize-space(@title)=\'Close\' and normalize-space(@aria-label)=\'Close\' and normalize-space(@role)=\'button\']/span');
  */
  // Assert Search Pop Up is correct
  // const searchBoxText = await page.textContent('[class="esri-search-result-renderer__more-results-item"]');
  // expect(searchBoxText === 'Atlanta, Georgia');
});
