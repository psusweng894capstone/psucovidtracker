"""
test_cdc_api.py.

These are the tests for the CDC COVID-19 data API.
"""
import numpy as np
import pandas as pd

from app.api import cdc_api


# submission_date used to produce results : "2020-04-01T00:00:00.000"
def example_cdc_data():
    """Test CDC COVID-19 data."""
    test_data = [
        {'state': 'CO', 'new_case': '376.0', 'new_death': '11.0'},
        {'state': 'FL', 'new_case': '1005.0', 'new_death': '16.0'},
        {'state': 'AZ', 'new_case': '124.0', 'new_death': '5.0'},
        {'state': 'SC', 'new_case': '210.0', 'new_death': '4.0'},
        {'state': 'CT', 'new_case': '429.0', 'new_death': '16.0'},
        {'state': 'NE', 'new_case': '37.0', 'new_death': '1.0'},
        {'state': 'IA', 'new_case': '52.0', 'new_death': '2.0'},
        {'state': 'NM', 'new_case': '48.0', 'new_death': '1.0'},
        {'state': 'KY', 'new_case': '89.0', 'new_death': '2.0'},
        {'state': 'WY', 'new_case': '17.0', 'new_death': '0.0'},
        {'state': 'ND', 'new_case': '21.0', 'new_death': '0.0'},
        {'state': 'WA', 'new_case': '423.0', 'new_death': '51.0'},
        {'state': 'RMI', 'new_case': '0.0', 'new_death': '0.0'},
        {'state': 'TN', 'new_case': '1.0', 'new_death': '0.0'},
        {'state': 'AS', 'new_case': '0.0', 'new_death': '0.0'},
        {'state': 'MA', 'new_case': '1118.0', 'new_death': '33.0'},
        {'state': 'PA', 'new_case': '962.0', 'new_death': '11.0'},
        {'state': 'NYC', 'new_case': '6691.0', 'new_death': '301.0'},
        {'state': 'OH', 'new_case': '348.0', 'new_death': '10.0'},
        {'state': 'AL', 'new_case': '107.0', 'new_death': '4.0'},
        {'state': 'VA', 'new_case': '222.0', 'new_death': '7.0'},
        {'state': 'MI', 'new_case': '1719.0', 'new_death': '78.0'},
        {'state': 'CA', 'new_case': '24.0', 'new_death': '0.0'},
        {'state': 'MS', 'new_case': '104.0', 'new_death': '4.0'},
        {'state': 'IL', 'new_case': '986.0', 'new_death': '42.0'},
        {'state': 'NJ', 'new_case': '3559.0', 'new_death': '88.0'},
        {'state': 'TX', 'new_case': '731.0', 'new_death': '17.0'},
        {'state': 'WI', 'new_case': '199.0', 'new_death': '8.0'},
        {'state': 'GA', 'new_case': '570.0', 'new_death': '24.0'},
        {'state': 'LA', 'new_case': '1187.0', 'new_death': '34.0'},
        {'state': 'NV', 'new_case': '166.0', 'new_death': '6.0'},
        {'state': 'PR', 'new_case': '47.0', 'new_death': '3.0'},
        {'state': 'IN', 'new_case': '406.0', 'new_death': '16.0'},
        {'state': 'OK', 'new_case': '154.0', 'new_death': '7.0'},
        {'state': 'OR', 'new_case': '46.0', 'new_death': '0.0'},
        {'state': 'MD', 'new_case': '325.0', 'new_death': '13.0'},
        {'state': 'NY', 'new_case': '3617.0', 'new_death': '64.0'},
        {'state': 'NC', 'new_case': '86.0', 'new_death': '1.0'},
        {'state': 'ID', 'new_case': '144.0', 'new_death': '0.0'},
        {'state': 'UT', 'new_case': '78.0', 'new_death': '2.0'},
        {'state': 'AR', 'new_case': '54.0', 'new_death': '2.0'},
        {'state': 'MO', 'new_case': '254.0', 'new_death': '4.0'},
        {'state': 'DE', 'new_case': '49.0', 'new_death': '1.0'},
        {'state': 'MN', 'new_case': '53.0', 'new_death': '1.0'},
        {'state': 'WV', 'new_case': '29.0', 'new_death': '1.0'},
        {'state': 'RI', 'new_case': '46.0', 'new_death': '2.0'},
        {'state': 'DC', 'new_case': '91.0', 'new_death': '2.0'},
        {'state': 'KS', 'new_case': '54.0', 'new_death': '1.0'},
        {'state': 'ME', 'new_case': '41.0', 'new_death': '2.0'},
        {'state': 'SD', 'new_case': '21.0', 'new_death': '1.0'},
        {'state': 'NH', 'new_case': '48.0', 'new_death': '1.0'},
        {'state': 'MT', 'new_case': '25.0', 'new_death': '0.0'},
        {'state': 'HI', 'new_case': '28.0', 'new_death': '0.0'},
        {'state': 'AK', 'new_case': '10.0', 'new_death': '0.0'},
        {'state': 'VT', 'new_case': '28.0', 'new_death': '3.0'},
        {'state': 'GU', 'new_case': '6.0', 'new_death': '1.0'},
        {'state': 'VI', 'new_case': '0.0', 'new_death': '0.0'},
        {'state': 'MP', 'new_case': '6.0', 'new_death': '0.0'},
        {'state': 'FSM', 'new_case': '0.0', 'new_death': '0.0'},
        {'state': 'PW', 'new_case': '0.0', 'new_death': '0.0'}
    ]

    return pd.DataFrame.from_records(test_data)


def test_cdc_api():
    """Unit test to ensure API data returned matches what is expected."""
    # Arrange
    expected = example_cdc_data().sort_values("state", ignore_index=True)
    # print(expected)

    # Act
    results = cdc_api.get_cdc_data(2020, 4, 1).sort_values("state",
                                                           ignore_index=True)
    # print(results)

    # Assert
    pd.testing.assert_frame_equal(results, expected)


def test_cdc_api_bad_input():
    """Unit test to ensure API endpoint can handle bad input."""
    # Arrange
    year = -2020
    month = 1
    day = 5

    # Act
    results = cdc_api.get_cdc_data(year, month, day)

    # Assert
    assert (results is None)
