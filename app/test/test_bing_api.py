"""
test_bing_api.py.

These are the tests for the Bing COVID-19 data API.
"""
import pandas as pd

from app.api import bing_api


# Global bing_data() to speed up test runs
test_object = bing_api.bing_data(test=True)


def test_bing_api_constructor():
    """Test the Bing API constructor."""
    # test_object = bing_api.bing_data()
    expected_columns = ['Updated', 'Confirmed', 'new_cases',
                        'Deaths', 'new_deaths',  'Recovered',
                        'RecoveredChange', 'lat', 'lon',
                        'Country', 'Region', 'Locality',
                        'rolling_deaths', 'rolling_cases']
    assert((test_object.df.columns == expected_columns).all())


def test_data_pull():
    """Test whether Bing data can be pulled."""
    # test_object = bing_api.bing_data()
    valid_df = test_object.df

    invalid_url = \
        'https://github.com/microsoft/Bing-COVID-19-Data/raw/master/'\
        'data/Bing-COVID19-Data.txt'
    test_object.link = invalid_url
    test_object._data_pull(test=False)
    assert(valid_df.equals(test_object.df))

    non_csv = \
        'https://github.com/microsoft/Bing-COVID-19-Data/raw/master/'\
        'README.md'
    test_object.link = non_csv
    test_object._data_pull(test=False)
    assert(valid_df.equals(test_object.df))


def test_calc_stats():
    """Test whether object can calculate statistics."""
    assert(test_object.df['rolling_deaths'].dtypes == 'float64')
    assert(test_object.df['rolling_deaths'].dtypes == 'float64')


def test_calc_rolling_average():
    """Test rolling average algorithm is correct."""
    # Save existing DF
    old_df = test_object.df

    test_data = [
                  ['2020-11-15', 'United States', 'Pennsylvania',
                   'Philadelphia County', 0.0],
                  ['2020-11-16', 'United States', 'Pennsylvania',
                   'Philadelphia County', 1.0],
                  ['2020-11-17', 'United States', 'Pennsylvania',
                   'Philadelphia County', 4.0],
                  ['2020-11-18', 'United States', 'Pennsylvania',
                   'Philadelphia County', 12.0],
                  ['2020-11-19', 'United States', 'Pennsylvania',
                   'Philadelphia County', 10.0]
                  ]
    test_columns = ['Updated', 'Country', 'Region', 'Locality', 'new_deaths']
    test_df = pd.DataFrame(data=test_data, columns=test_columns)

    # inject test_df into test_object
    test_object.df = test_df

    result = test_object._calc_rolling_average(column='new_deaths', period=2)

    expected_result = pd.Series(['NaN', 0.5, 2.5, 8.0, 11.0], dtype='float64')

    assert(result.equals(expected_result))

    # restore test_object
    test_object.df = old_df


def test_df_to_geojson():
    """Test to see if the dataframe can be converted to GeoJSON."""
    # test_object = bing_api.bing_data()
    # Get a subset of complete COVID data to speed up test
    test_df = test_object.df[
                             test_object.df['Updated'] ==
                             test_object.get_most_recent_date()
                             ]
    geojson = test_object.df_to_geojson(test_df)
    expected_keys = ['type', 'features']
    assert(list(geojson) == expected_keys)
    assert(geojson['type'] == 'FeatureCollection')
    assert(len(geojson['features']) > 1)


def test_get_regions():
    """Test if the region data can be obtained."""
    regions = test_object.get_regions()
    expected_columns = ['Country', 'Region', 'Locality', 'lat', 'lon']
    assert((regions.columns == expected_columns).all())


def test_get_most_recent_date():
    """Test if the data from the most recent date can be obtained."""
    # test_object = bing_api.bing_data()
    assert(isinstance(test_object.get_most_recent_date(),
           pd._libs.tslibs.timestamps.Timestamp))


def test_get_oldest_date():
    """Test if the data from the oldest date can be obtained."""
    # test_object = bing_api.bing_data()
    assert(isinstance(test_object.get_oldest_date(),
           pd._libs.tslibs.timestamps.Timestamp))


def test_get_categories():
    """Test if the category data can be obtained."""
    # test_object = bing_api.bing_data()
    expected_cat = ['Updated', 'Confirmed', 'new_cases',
                    'Deaths', 'new_deaths',  'Recovered',
                    'RecoveredChange', 'lat', 'lon',
                    'Country', 'Region', 'Locality',
                    'rolling_deaths', 'rolling_cases']
    assert((test_object.get_categories() == expected_cat).all())


def test_get_geojson_data():
    """Test if the geojson data can be obtained."""
    geojson = test_object.get_geojson_data()
    expected_keys = ['type', 'features']
    assert(list(geojson) == expected_keys)
    assert(geojson['type'] == 'FeatureCollection')
    assert(len(geojson['features']) > 0)


def test_get_most_recent_data():
    """Test if the most recent data can be obtained from the dataframe."""
    df = test_object.get_most_recent_data()
    expected_cols = ['Country', 'Region', 'Locality', 'new_cases',
                     'new_deaths', 'Recovered', 'Confirmed', 'Deaths', 'lat',
                     'lon', 'Updated',
                     'Day 0', 'Day 1',
                     'Day 2', 'Day 3', 'Day 4']
    assert((df.columns == expected_cols).all())

    assert(df.duplicated(keep=False).sum() == 0)


def test_get_region_data():
    """Test if the region data can be obtained from the GeoJSON object."""
    geojson = test_object.get_region_data()
    expected_keys = ['type', 'features']
    assert(list(geojson) == expected_keys)
    assert(geojson['type'] == 'FeatureCollection')
    assert(len(geojson['features']) > 0)


def test_get_country_data():
    """Test if the country data can be obtained from the GeoJSON object."""
    geojson = test_object.get_country_data()
    expected_keys = ['type', 'features']
    assert(list(geojson) == expected_keys)
    assert(geojson['type'] == 'FeatureCollection')
    assert(len(geojson['features']) > 0)


def test_get_locality_data():
    """Test if the locality data can be obtained from the GeoJSON object."""
    geojson = test_object.get_locality_data()
    expected_keys = ['type', 'features']
    assert(list(geojson) == expected_keys)
    assert(geojson['type'] == 'FeatureCollection')
    assert(len(geojson['features']) > 0)


def test_print_dataframe():
    """Test that function does not throw any exceptions."""
    test_object.print_dataframe(test_object.df)
    # No assert statement is needed. Pytest will fail this test
    # if any exceptions are raised.
