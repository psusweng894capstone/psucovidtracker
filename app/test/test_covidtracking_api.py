"""
test_covidtracking_api.py.

These are the tests for the COVID Tracking Project API.
"""
import pandas as pd
import json

from app.api import covidtracking_api

# Global bing_data() to speed up test runs
test_object = covidtracking_api.ctp_data()


def test_covidtracking_api_constructor():
    """Test the COVID Tracking Project API constructor."""
    expected_columns = ['date', 'State', 'Positive Test', 'probableCases',
                        'negative', 'pending', 'totalTestResultsSource',
                        'Total Test Results', 'Currently Hospitalized',
                        'Total Hospitalized', 'inIcuCurrently',
                        'inIcuCumulative', 'onVentilatorCurrently',
                        'onVentilatorCumulative', 'recovered',
                        'dataQualityGrade', 'lastUpdateEt', 'dateModified',
                        'checkTimeEt', 'death', 'hospitalized', 'dateChecked',
                        'totalTestsViral', 'positiveTestsViral',
                        'negativeTestsViral', 'positiveCasesViral',
                        'deathConfirmed', 'deathProbable',
                        'totalTestEncountersViral', 'totalTestsPeopleViral',
                        'totalTestsAntibody', 'positiveTestsAntibody',
                        'negativeTestsAntibody', 'totalTestsPeopleAntibody',
                        'positiveTestsPeopleAntibody',
                        'negativeTestsPeopleAntibody',
                        'totalTestsPeopleAntigen',
                        'positiveTestsPeopleAntigen',
                        'totalTestsAntigen', 'positiveTestsAntigen', 'fips',
                        'positiveIncrease', 'negativeIncrease', 'total',
                        'totalTestResultsIncrease', 'posNeg', 'deathIncrease',
                        'hospitalizedIncrease', 'hash', 'commercialScore',
                        'negativeRegularScore', 'negativeScore',
                        'positiveScore', 'score', 'grade', 'Rate',
                        'Infection Fatality Ratio', 'Case Fatality Ratio']
    assert((test_object.df.columns == expected_columns).all())


def test_data_pull():
    """Test whether Bing data can be pulled."""
    valid_df = test_object.df

    invalid_url = \
        'https://api.covidtracking.com/v1/states/current.csvX'
    test_object.link = invalid_url
    test_object._data_pull()
    assert(valid_df.equals(test_object.df))

    non_csv = \
        'https://github.com/microsoft/Bing-COVID-19-Data/raw/master/'\
        'README.md'
    test_object.link = non_csv
    test_object._data_pull()
    assert(valid_df.equals(test_object.df))


def test_to_json():
    """Test json string produced by object."""
    state_json = test_object.to_json()

    state_map = json.loads(state_json)

    expected_keys = ['State', 'Currently Hospitalized', 'Total Hospitalized',
                     'Rate']

    assert list(state_map) == expected_keys


def test_case_fat_ratio():
    """Test Case Fatality Ratio calculation."""
    old_df = test_object.df
    test_data = [
        [142, 34041, 7165],
        [3831, 264199, 168387],
        [2586, 167137, 146496],
        [0, 0, ],
        [6885, 352101, 54980]
    ]
    test_columns = ['death', 'Positive Test', 'recovered']
    test_df = pd.DataFrame(data=test_data, columns=test_columns)

    # inject test_df into test_object
    test_object.df = test_df

    result = test_object._case_fat_ratio()

    expected_result = pd.Series([0.0034461000825122554,
                                 0.00885604249790793,
                                 0.008245305819221829,
                                 'NaN',
                                 0.01691309591948531],
                                dtype='float64')
    # Use pd.testing because it handles float comparisons gracefully
    pd.testing.assert_series_equal(result, expected_result)

    # restore test_object
    test_object.df = old_df


def test_inf_fat_ratio():
    """
    Test 'Infection Fatality Ratio'.

    Infection Fatality ratio = (Number of deaths from disease/
    Number of infected individuals)
    """
    old_df = test_object.df
    test_data = [
        [142, 34041],
        [3831, 264199],
        [2586, 167137],
        [0, 0, ],
        [6885, 352101]
    ]
    test_columns = ['death', 'Positive Test']
    test_df = pd.DataFrame(data=test_data, columns=test_columns)

    # inject test_df into test_object
    test_object.df = test_df

    result = test_object._inf_fat_ratio()

    expected_result = pd.Series([0.0041714403219646895,
                                 0.01450043338544052,
                                 0.015472337064803126,
                                 'NaN',
                                 0.01955404841224535],
                                dtype='float64')
    # Use pd.testing because it handles float comparisons gracefully
    pd.testing.assert_series_equal(result, expected_result)

    # restore test_object
    test_object.df = old_df


def test_cvd_rate():
    """Test 'COVID-19 Positivity Rate'."""
    old_df = test_object.df
    test_data = [
        [142, 1050369],
        [3831, 1626368],
        [2586, 1734686],
        [0, 2140],
        [6885, 2326744]
    ]
    test_columns = ['Positive Test', 'Total Test Results']
    test_df = pd.DataFrame(data=test_data, columns=test_columns)

    # inject test_df into test_object
    test_object.df = test_df

    result = test_object._cvd_rate()

    expected_result = pd.Series([0.0001351905854037962,
                                 0.002355555446245868,
                                 0.0014907597109793933,
                                 0.0,
                                 0.002959070701374969],
                                dtype='float64')
    # Use pd.testing because it handles float comparisons gracefully
    pd.testing.assert_series_equal(result, expected_result)

    # restore test_object
    test_object.df = old_df


def test_state_converter():
    """Test 'State Converter'."""
    old_df = test_object.df
    test_data = [
        ['AK'],
        ['AL'],
        ['AR'],
        ['AS'],
        ['AZ'],
        ['CA'],
        ['CO'],
        ['CT'],
        ['DC'],
        ['DE'],
        ['FL'],
        ['GA'],
        ['GU'],
        ['HI'],
        ['IA'],
        ['ID'],
        ['IL'],
        ['IN'],
        ['KS'],
        ['KY'],
        ['LA'],
        ['MA'],
        ['MD'],
        ['ME'],
        ['MI'],
        ['MN'],
        ['MO'],
        ['MP'],
        ['MS'],
        ['MT'],
        ['NC'],
        ['ND'],
        ['NE'],
        ['NH'],
        ['NJ'],
        ['NM'],
        ['NV'],
        ['NY'],
        ['OH'],
        ['OK'],
        ['OR'],
        ['PA'],
        ['PR'],
        ['RI'],
        ['SC'],
        ['SD'],
        ['TN'],
        ['TX'],
        ['UT'],
        ['VA'],
        ['VI'],
        ['VT'],
        ['WA'],
        ['WI'],
        ['WV'],
        ['WY']
    ]
    test_columns = ['State']
    test_df = pd.DataFrame(data=test_data, columns=test_columns)

    # inject test_df into test_object
    test_object.df = test_df

    result = test_object._state_converter()

    expected_result = pd.Series(['Alaska', 'Alabama', 'Arkansas',
                                 'American Samoa', 'Arizona', 'California',
                                 'Colorado', 'Connecticut',
                                 'District of Columbia', 'Delaware', 'Florida',
                                 'Georgia', 'Guam', 'Hawaii', 'Iowa', 'Idaho',
                                 'Illinois', 'Indiana', 'Kansas', 'Kentucky',
                                 'Louisiana', 'Massachusetts', 'Maryland',
                                 'Maine', 'Michigan', 'Minnesota', 'Missouri',
                                 'Northern Mariana Islands', 'Mississippi',
                                 'Montana', 'North Carolina', 'North Dakota',
                                 'Nebraska', 'New Hampshire', 'New Jersey',
                                 'New Mexico', 'Nevada', 'New York', 'Ohio',
                                 'Oklahoma', 'Oregon', 'Pennsylvania',
                                 'Puerto Rico', 'Rhode Island',
                                 'South Carolina', 'South Dakota',
                                 'Tennessee', 'Texas', 'Utah', 'Virginia',
                                 'Virgin Islands', 'Vermont', 'Washington',
                                 'Wisconsin', 'West Virginia', 'Wyoming'])
    assert (result.equals(expected_result))
