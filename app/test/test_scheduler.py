"""
test_scheduler.py.

These are the tests for the data refresh scheduler.
"""
from app.main import refresh_data


def test_refresh_data():
    """Test the refresh data function."""
    refresh_data(test=True)
