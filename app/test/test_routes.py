"""
test_routes.py.

These are the tests for the route endpoints.
"""
from app import main
import pytest
import datetime


# Global Flask App to test
@pytest.fixture
def client():
    """
    Create a test_client object.

    Yields:
        test_client: An object used to test data responses
    """
    main.app.config['TESTING'] = True
    with main.app.test_client() as client:
        yield client


def test_index(client):
    """
    Test for the response from the index data.

    Args:
        client (test_client): An object used to test data responses
    """
    response = client.get('/')
    assert(response.status_code == 200)


def test_get_state_data(client):
    """
    Test for the response to the state data.

    Args:
        client (test_client): An object used to test data responses
    """
    response = client.get('/PA')
    assert(response.status_code == 200)


def test_get_cdc_api(client):
    """
    Test for the response to the cdc api data.

    Args:
        client (test_client): An object used to test data responses
    """
    response = client.get('/api/cdc')
    assert(response.status_code == 200)


def test_api_regions(client):
    """
    Test for the response to the api regions data.

    Args:
        client (test_client): An object used to test data responses
    """
    response = client.get('/api/regions')
    assert(response.status_code == 200)
    assert(response.get_json(silent=True) ==
           main.covid_data.get_regions().fillna('').to_dict(orient='records')
           )


def test_date_range(client):
    """
    Test for the response to the date range in the data.

    Args:
        client (test_client): An object used to test data responses
    """
    response = client.get('/api/daterange')
    assert(response.status_code == 200)
    assert(response.is_json)
    daterange = response.get_json()
    start_date = datetime.date(year=daterange['start_date']['year'],
                               month=daterange['start_date']['month'],
                               day=daterange['start_date']['day'])
    end_date = datetime.date(year=daterange['end_date']['year'],
                             month=daterange['end_date']['month'],
                             day=daterange['end_date']['day'])
    assert(start_date < end_date)


def test_get_categories(client):
    """
    Test for the response to the categories in the data.

    Args:
        client (test_client): An object used to test data responses
    """
    response = client.get('/api/categories')
    assert(response.status_code == 200)
    expected_cat = main.covid_data.get_categories()
    assert((response.get_json() == expected_cat).all())


def test_get_geojson(client):
    """
    Test for the response to the geojson locality data.

    Args:
        client (test_client): An object used to test data responses
    """
    response = client.get('/api/dataset.geojson')
    assert(response.status_code == 200)
    assert(response.get_json())


def test_get_geojson_locality(client):
    """
    Test for the response to the GeoJSON locality data.

    Args:
        client (test_client): An object used to test data responses
    """
    response = client.get('/api/locality.geojson')
    assert(response.status_code == 200)
    assert(response.get_json())


def test_get_geojson_country(client):
    """
    Test for the response to the GeoJSON country data.

    Args:
        client (test_client): An object used to test data responses
    """
    response = client.get('/api/country.geojson')
    assert(response.status_code == 200)
    assert(response.get_json())


def test_get_geojson_region(client):
    """
    Test for the response to the GeoJSON region data.

    Args:
        client (test_client): An object used to test data responses
    """
    response = client.get('/api/region.geojson')
    assert(response.status_code == 200)
    assert(response.get_json())


def test_get_chart_data(client):
    """
    Test for the response to the GeoJSON chart data.

    Args:
        client (test_client): An object used to test data responses
    """
    response = client.get('/api/new_deaths/3/agg_deaths')
    assert(response.status_code == 200)


# def test_get_json_states(client):
#     """
#     Test '/api/states.json' route is working.
#
#     Args:
#         client (test_client): An object used to test data responses
#     """
#     response = client.get('/api/states.json')
#     assert(response.status_code == 200)
