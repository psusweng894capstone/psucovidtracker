"""
covidtracking_api.py.

The main API for processing The COVID Tracking Project data.
"""
import pandas as pd
import urllib


class ctp_data:
    """Download and process COVID-19 data from The COVID Tracking Project.

    https://covidtracking.com/data/api
    """

    def __init__(self):
        """
        Parse COVID-19 data from Bing into a pandas dataframe.

        :returns: A new covidtracking_data object
        """
        self.link = 'https://api.covidtracking.com/v1/states/daily.csv'
        self.df = pd.DataFrame()
        self.update()

    def update(self):
        """
        Update the COVID-19 data internal to this object.

        :returns: Nothing
        """
        self._data_pull()
        self.df['Rate'] = self._cvd_rate()
        self.df['Infection Fatality Ratio'] = self._inf_fat_ratio().round(3)
        self.df['Case Fatality Ratio'] = self._case_fat_ratio().round(3)
        self.df['State'] = self._state_converter()

    def _data_pull(self):
        # Read in csv
        try:
            df = pd.read_csv(self.link,
                             parse_dates=['date'], cache_dates=True,
                             infer_datetime_format=True,)
        except urllib.error.HTTPError:
            return
        except ValueError:
            return

        df.rename(columns={
                        'state': 'State',
                        'positive': 'Positive Test',
                        'totalTestResults': 'Total Test Results',
                        'hospitalizedCurrently': 'Currently Hospitalized',
                        'hospitalizedCumulative': 'Total Hospitalized',
                        'dataQuality': 'Data Quality'},
                  inplace=True)
        self.df = df

    def _cvd_rate(self):
        # Add column representing COVID-19 infection rate
        return (self.df['Positive Test'] /
                self.df['Total Test Results'])

    def _state_converter(self):
        us_state_dict = {
            'AK': 'Alaska',
            'AL': 'Alabama',
            'AR': 'Arkansas',
            'AS': 'American Samoa',
            'AZ': 'Arizona',
            'CA': 'California',
            'CO': 'Colorado',
            'CT': 'Connecticut',
            'DC': 'District of Columbia',
            'DE': 'Delaware',
            'FL': 'Florida',
            'GA': 'Georgia',
            'GU': 'Guam',
            'HI': 'Hawaii',
            'IA': 'Iowa',
            'ID': 'Idaho',
            'IL': 'Illinois',
            'IN': 'Indiana',
            'KS': 'Kansas',
            'KY': 'Kentucky',
            'LA': 'Louisiana',
            'MA': 'Massachusetts',
            'MD': 'Maryland',
            'ME': 'Maine',
            'MI': 'Michigan',
            'MN': 'Minnesota',
            'MO': 'Missouri',
            'MP': 'Northern Mariana Islands',
            'MS': 'Mississippi',
            'MT': 'Montana',
            'NC': 'North Carolina',
            'ND': 'North Dakota',
            'NE': 'Nebraska',
            'NH': 'New Hampshire',
            'NJ': 'New Jersey',
            'NM': 'New Mexico',
            'NV': 'Nevada',
            'NY': 'New York',
            'OH': 'Ohio',
            'OK': 'Oklahoma',
            'OR': 'Oregon',
            'PA': 'Pennsylvania',
            'PR': 'Puerto Rico',
            'RI': 'Rhode Island',
            'SC': 'South Carolina',
            'SD': 'South Dakota',
            'TN': 'Tennessee',
            'TX': 'Texas',
            'UT': 'Utah',
            'VA': 'Virginia',
            'VI': 'Virgin Islands',
            'VT': 'Vermont',
            'WA': 'Washington',
            'WI': 'Wisconsin',
            'WV': 'West Virginia',
            'WY': 'Wyoming'
        }
        # Apply state dictioary directly to dataframe
        return self.df['State'].map(us_state_dict)

    def _inf_fat_ratio(self):
        """
        Calculate 'Infection Fatality Ratio'.

        Infection Fatality ratio = (Number of deaths from disease/
        Number of infected individuals)*100
        """
        return self.df['death'] / self.df['Positive Test']

    def _case_fat_ratio(self):
        """
        Calculate 'Case Fatality Ratio'.

        Case Fatality Ratio = Number of deaths from disease
        /(no of deaths from disease + number of recovered from disease)*100
        """
        return (self.df['death'] /
                (self.df['Positive Test'] + self.df['recovered']))

    def to_json(self):
        """
        Generate JSON object with state hospitaliation statistics.

        :returns: json object
        """
        df_2 = (self.df[['State', 'Currently Hospitalized',
                'Total Hospitalized', 'Rate']])
        df_2 = df_2.fillna(0)
        return df_2.to_json()
