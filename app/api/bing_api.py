"""
bing_api.py.

The main API for pulling Bing COVID-19 data.
"""
# !/usr/bin/env python

# make sure to install these packages before running:
# pip install pandas

import pandas as pd
import numpy as np
import urllib
import requests
import io
import datetime
from time import perf_counter
from . import covidtracking_api


def concat(series):
    """Take a series and return an aggregated array string."""
    strings = ['%.0f' % x for x in series]
    return ','.join(strings)


class bing_data:
    """Download and digest COVID-19 data from Microsft Bing repository."""

    def __init__(self, test=False):
        """
        Parse COVID-19 data from Bing into a pandas dataframe.

        :param test: Flag indicating whether function is run in a test mode.
            (Default is false)
        :returns: A new bing_data object
        """
        self.link = 'https://github.com/microsoft/Bing-COVID-19-Data/raw/' \
                    'master/data/Bing-COVID19-Data.csv'
        self.fillna_map = {'Confirmed': 0, 'new_cases': 0, 'Deaths': 0,
                           'new_deaths': 0, 'Recovered': 0,
                           'RecoveredChange': 0, 'lat': 0, 'lon': 0,
                           'Country': '', 'Region': '', 'Locality': '',
                           'Case Fatality Ratio': 0,
                           'Currently Hospitalized': 0,
                           'Infection Fatality Ratio': 0,
                           'Rate': 0
                           }
        self.df = pd.DataFrame()
        self.most_recent_df = pd.DataFrame()
        self.ctp = covidtracking_api.ctp_data()
        self.ctp_df = pd.DataFrame()
        self.update(test)

    def update(self, test=False):
        """
        Update the bing_data object.

        Args:
            test (bool, optional): Defaults to False.
        """
        tic = perf_counter()
        self._data_pull(test)
        self._calc_stats()
        self.ctp.update()
        self._transform_ctp_data()
        toc = perf_counter()
        test_string = 'test' if test else ''
        print(f"Downloaded and processed the {test_string} DF data in"
              f" {toc - tic:0.4f}s")

        tic = perf_counter()
        self.most_recent_df = self.get_most_recent_data()
        toc = perf_counter()
        print(f"Extracted the most recent data in {toc - tic:0.4f}s")

    def _data_pull(self, test=False):
        """
        Download and process the data from the Bing COVID-19 repository.

        Args:
            test (bool, optional):  Defaults to False.
        """
        # Read in csv
        try:
            if test:
                # Download the first 1mb of the bing data
                headers = {'Range': 'bytes0-1048576'}
                r = requests.get(self.link, headers=headers)
                covid_data = io.BytesIO(r.content)
            else:
                covid_data = self.link
            df = pd.read_csv(covid_data,
                             parse_dates=['Updated'], cache_dates=True,
                             infer_datetime_format=True,
                             usecols=['Updated', 'Confirmed',
                                      'ConfirmedChange', 'Deaths',
                                      'DeathsChange', 'Recovered',
                                      'RecoveredChange', 'Latitude',
                                      'Longitude', 'Country_Region',
                                      'AdminRegion1', 'AdminRegion2'
                                      ],
                             low_memory=False
                             )
        except urllib.error.HTTPError:
            return
        except ValueError:
            return

        df.rename(columns={'Country_Region': 'Country',
                           'AdminRegion1': 'Region',
                           'AdminRegion2': 'Locality',
                           'Latitude': 'lat',
                           'Longitude': 'lon',
                           'DeathsChange': 'new_deaths',
                           'ConfirmedChange': 'new_cases'},
                  inplace=True)
        # Filter out bad values
        df = df[df['new_deaths'] >= 0]
        df = df[df['new_cases'] >= 0]

        self.df = df

    def _calc_stats(self):
        self.df['rolling_deaths'] = self._calc_rolling_average(
            column='new_deaths')
        self.df['rolling_cases'] = self._calc_rolling_average(
            column='new_cases')

    def _calc_rolling_average(self, column, period=7):
        return self.df.groupby(['Country', 'Region', 'Locality']
                               ).rolling(period)[column].mean().reset_index(
                                                                    drop=True
                                                                    )

    # Convert pandas dataframe to geojson structure,
    # removing long/lat from properties, and
    # placing them into geometry.
    # Main data structure for ArcGIS to absorb data.
    def df_to_geojson(self, df):
        """
        Convert the dataframe object to a GeoJSON object.

        Args:
            df (dataframe): A dataframe object containing the api data

        Returns:
            geojson: A geojson object containing the api data
        """
        # Fill in any NaNs in the DF since they don't translate into JSON well
        df = df.fillna(self.fillna_map)

        # Need to convert the date to seconds from epoch for
        # ArcGIS time slider to work
        # df['Updated'] = (df['Updated'] - datetime.datetime(1970, 1, 1)) \
        #                  .dt.total_seconds()

        prop_dict = df.to_dict(orient='records')

        feature_list = [
            {'type': 'Feature',
             'properties': dict((k, v) for k, v in item.items()
                                if k not in ['lon', 'lat']
                                ),
             'geometry': {'type': 'Point',
                          'coordinates': [item['lon'],
                                          item['lat']
                                          ]
                          }
             } for item in prop_dict
        ]

        geojson = {'type': 'FeatureCollection',
                   'features': feature_list}

        return geojson

    def get_regions(self):
        """
        Get DataFrame of regions contaiained in bing_data.

        Returns:
            dataframe: A dataframe object containing the region data
        """
        return self.df[
            ['Country', 'Region', 'Locality', 'lat', 'lon']
        ].drop_duplicates()

    def get_most_recent_date(self):
        """
        Get the the most recent date from the object.

        Returns:
            datetime: A datetime object containing most recent data
        """
        return self.df['Updated'].max()

    def get_oldest_date(self):
        """
        Get the the oldest date from the API object.

        Returns:
            datetime: A datetime object containing oldest data
        """
        return self.df['Updated'].min()

    def get_categories(self):
        """
        Get the columns in bing_data.

        Returns:
            list: A list of columns
        """
        return self.df.columns

    def get_most_recent_data(self):
        """
        Get the most recent data from the API object.

        Returns:
            dataframe: A dataframe object containing the most recent data
        """
        df = self.df.sort_values('Updated')
        df = df.drop_duplicates(['Country', 'Region', 'Locality'],
                                keep='last')

        # Add string array to new column 'Line_Chart'
        df_temp = self.get_aggregate_data(column='new_cases',
                                          num_pts=5,
                                          label='Line_Chart')
        df = self.join_aggregate_data(df, df_temp)
        # self.print_dataframe(df)

        cols = ['Country', 'Region', 'Locality', 'new_cases', 'new_deaths',
                'Recovered', 'Confirmed', 'Deaths', 'lat', 'lon', 'Updated',
                'Day 0', 'Day 1', 'Day 2', 'Day 3', 'Day 4']

        df = df[cols]
        return df

    def get_geojson_data(self):
        """
        Get the GeoJSON data from the API object.

        Returns:
            geojson: A GeoJSON object containing the API data
        """
        return self.df_to_geojson(self.df)

    def get_region_data(self):
        """
        Get the GeoJSON region data from the API object.

        Returns:
            geojson: A GeoJSON object containing the region data
        """
        df = self.most_recent_df
        df = df[(df['Locality'].isna()) & (df['Region'].notna())]
        df = self._merge_ctp_data(df)
        # self.print_dataframe(df)
        geojson = self.df_to_geojson(df)
        return geojson

    def get_country_data(self):
        """
        Get the GeoJSON country data from the API object.

        Returns:
            geojson: A GeoJSON object containing the country data
        """
        df = self.most_recent_df
        df = df[(df['Locality'].isna()) & (df['Region'].isna())
                & (df['Country'] != 'Worldwide')]
        geojson = self.df_to_geojson(df)
        return geojson

    def get_locality_data(self):
        """
        Get the GeoJSON locality data from the API object.

        Returns:
            geojson: A GeoJSON object containing the locality data
        """
        df = self.most_recent_df
        df = df[(df['Locality'].notna()) & (df['Region'].notna())]
        geojson = self.df_to_geojson(df)
        return geojson

    def get_aggregate_data(self, column, num_pts, label):
        """
        Aggregate most recent 'num_pts' of 'column' data into an array string.

        :return: a new dataframe
        """
        group = ['Country', 'Region', 'Locality', 'lat', 'lon']

        df = self.df.dropna(subset=[column])
        df = df.sort_values('Updated').groupby(group, dropna=False)
        # print(df)
        agg = df.tail(num_pts).groupby(group,
                                       dropna=False)[column].apply(concat)
        # print(agg)
        df = agg.to_frame().reset_index()
        # print(df)
        df.rename(columns={column: label},
                  inplace=True)
        temp_df = df[label].str.split(pat=",", expand=True)
        result = pd.concat([temp_df, df], axis=1, sort=False)
        result.rename(columns={0: 'Day 0',
                               1: 'Day 1',
                               2: 'Day 2',
                               3: 'Day 3',
                               4: 'Day 4'},
                      inplace=True)
        # result.drop(columns='Line_Chart')
        return result

    def join_aggregate_data(self, left_df, right_df):
        """
        Join two dataframes after a return of the aggregate function.

        :return: a new dataframe
        """
        return pd.merge(left_df, right_df, on=['Country', 'Region', 'Locality',
                                               'lat', 'lon'])

    def print_dataframe(self, df):
        """
        Print a dataframe is clean format (for error checking).

        :return: N/A
        """
        # more options can be specified also
        # print(list(df.columns))
        print(type(df))
        with pd.option_context('display.max_rows', 10,
                               'display.max_columns', None):
            print(df)
        return

    def _merge_ctp_data(self, df):
        return df.merge(self.ctp_df, how='left', on=['Updated', 'Country',
                                                     'Region', 'Locality'])

    def _transform_ctp_data(self):
        self.ctp_df = self.ctp.df.rename(columns={'date': 'Updated',
                                                  'State': 'Region'})

        self.ctp_df = self.ctp_df[['Updated', 'Region', 'Rate',
                                   'Currently Hospitalized',
                                   'Infection Fatality Ratio',
                                   'Case Fatality Ratio']]

        self.ctp_df['Country'] = 'United States'
        self.ctp_df['Locality'] = np.NaN
        self.ctp_df['Locality'] = self.ctp_df['Locality'].astype('object')
