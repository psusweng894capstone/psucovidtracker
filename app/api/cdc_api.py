"""
cdc_api.py.

The main API for pulling CDC COVID-19 data.
"""
# !/usr/bin/env python

# make sure to install these packages before running:
# pip install pandas
# pip install sodapy

import datetime
import pandas as pd
from sodapy import Socrata
# from app.api import bing_api


def get_cdc_data(year, month, day):
    """
    Get the cdc data for a specific date.

    Args:
        year (date): the year
        month (date): the month
        day (date): the day

    Returns: dataframe: A dataframe containing the data for the date provided
    """
    # Unauthenticated client only works with public data sets. Note 'None'
    # in place of application token, and no username or password:
    client = Socrata("data.cdc.gov", None)

    # Example authenticated client (needed for non-public datasets):
    # client = Socrata(data.cdc.gov,
    #                  MyAppToken,
    #                  userame="user@example.com",
    #                  password="AFakePassword")

    # Return COVID-19 Data about cases and deaths by a specific date,
    # for all US states.
    # Sources:
    # https://dev.socrata.com/foundry/data.cdc.gov/9mfq-cb36
    # http://api.us.socrata.com/api/catalog/v1?ids=9mfq-cb36
    # https://github.com/xmunoz/sodapy#getdataset_identifier-content_typejson-kwargs
    # https://dev.socrata.com/docs/queries/

    try:
        timestamp = datetime.datetime(year, month, day)
    except ValueError:
        print("Invalid date parameter entered to CDC API")
        return

    # submission_date example : "2020-09-18T00:00:00.000"
    data_date = timestamp.strftime("%Y-%m-%dT00:00:00.000")
    results = client.get("9mfq-cb36",
                         submission_date=data_date,
                         select="state, new_case, new_death")

    # Convert to pandas DataFrame
    results_df = pd.DataFrame.from_records(results)
    # print(results_df.to_string())
    # more options can be specified also
    # with pd.option_context('display.max_rows', None,
    # 'display.max_columns', None):
    # print(results_df)

    client.close()

    return results_df
