FROM tiangolo/uwsgi-nginx-flask:python3.8

COPY ./app /app

COPY ./venv_requirements.txt venv_requirements.txt

RUN apt-get update -y && apt-get install ca-certificates -y && apt-get autoremove -y 

ENV PIP_DISABLE_PIP_VERSION_CHECK=1
ENV PIP_NO_CACHE_DIR=1

RUN pip install -r venv_requirements.txt