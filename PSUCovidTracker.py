"""
cdc_api.py.

The start of the application.
"""
from app.main import app

if __name__ == '__main__':
    app.run(debug=True, use_reloader=False)
